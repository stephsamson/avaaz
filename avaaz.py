import re  # the python regex module for is_valid_email


def extract_evens(list_of_ints):
    """
    Takes a list of ints and returns a new list with
    only the even numbers.
    """
    output = []

    for i in list_of_ints:
        if i % 2 == 0:
            output.append(i)
    return output


def is_valid_email(email):
    """
    Takes a string of an email and returns True if it follows
    [some alphanumeric characters]@[some alphanumeric characters].[com or org or net]
    otherwise it returns False.
    """
    email_pattern = re.compile('\w+@\w+\.(com|org|net)')
    if email_pattern.match(email):
        return True
    return False
