import unittest
from avaaz import extract_evens, is_valid_email


class TestAvaazExercises(unittest.TestCase):

    def test_extract_evens(self):
        L = [1, 2, 3, 4]
        M = [1, 1, 1, 1]
        N = [42]
        self.assertEqual(extract_evens(L), [2, 4])
        self.assertEqual(extract_evens(M), [])
        self.assertEqual(extract_evens(N), [42])

    def test_is_valid_email(self):
        com = 'steph123@123gmail.com'
        org = '42member@charity.org'
        net = 'jane37doe@site23.net'
        co = 'invalid@website.co'
        self.assertEqual(is_valid_email(com), True)
        self.assertEqual(is_valid_email(org), True)
        self.assertEqual(is_valid_email(net), True)
        self.assertEqual(is_valid_email(co), False)
        